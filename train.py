# from scipy import misc
# import matplotlib.pyplot as plt
#########################################
from PIL import Image
import matplotlib.image as mpimg
import Augmentor
import pandas as pd
########################################
import os
from sklearn import model_selection
import shutil
import numpy as np
import tensorflow as tf
import time
import math


BATCH_SIZE = 10
IMAGE_SIZE = 101
VALIDATION_RATIO = 0.2


# date layout is [index, how many ones, index, how many ones......], even index: start position of one, odd index: how many consecutive ones
# note that the starting index is 1, not 0
# example: 10010000111, rle encoding = 1 1, 4 1,  9 3
def rle_encoding(img):
    # along column
    pixels = img.flatten('F')
    # pad zero because the we want our first finding index is where 0->1, and last index is where 1->0(see below)
    pixels = np.concatenate([[0], pixels, [0]])
    # find index where current number is not equaling to previous number
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    # runs[1] = runs[1] - runs[0], runs[3] = runs[3] - runs[2]....... this procedure is calculating the number of consecutive ones
    runs[1::2] -= runs[::2]
    return runs


# def read_image(image_path, input_channel):
#     list = os.listdir(image_path)
#     number_files = len(list)
#     # number_files = 1
#     counter = 0
#     dataset = np.zeros(shape=[number_files, IMAGE_SIZE, IMAGE_SIZE, input_channel])
#     for filename in list:
#         image_name = (filename[:-4])
#         img = mpimg.imread(image_path + "\\" + filename)
#         if (img.ndim == 2):
#             img = np.expand_dims(img, axis = 2)
#         dataset[counter] = img
#         counter += 1
#         # print("This is no.%s image, image_name is %s" %(counter, image_name))
#         # imgplot = plt.imshow(img)
#         # plt.show()
#         # if (counter == number_files):
#     return np.reshape(dataset[:, :, :, 0], (number_files, IMAGE_SIZE, IMAGE_SIZE, 1))
#

def create_model(x, keep_prob, bn, training, load):
    def get_initial(shape, name, load_option):
        if load_option:
            return tf.get_variable(name = name, shape = shape)
        else:
            return tf.get_variable(name = name, shape = shape, initializer = tf.keras.initializers.glorot_uniform())

    def get_initial_bias(shape, name, load_option):
        if load_option:
            return tf.get_variable(name = name, shape = shape)
        else:
            return tf.get_variable(name = name, shape = shape, initializer=tf.zeros_initializer())

    def batch_norm(x, training):
        return tf.contrib.layers.batch_norm(x, center = True, scale = True, is_training = training, decay = 0.90)

    def conv2d(x, kernel_shape, stride_size, pad, name, need_bn, in_training, load_option):
        kernel = get_initial(shape = kernel_shape, name = name, load_option = load_option)
        bias = get_initial_bias(shape = kernel_shape[3], name = name + "_bias", load_option = load_option)
        result = tf.nn.conv2d(x, kernel, strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def transpose_conv(x, kernel_shape, stride_size, out_tensor, pad, name, need_bn, in_training, load_option):
        out_shape = out_tensor.get_shape().as_list()
        batch_size = tf.shape(x)[0]
        deconv_shape = tf.stack([batch_size, out_shape[1], out_shape[2], out_shape[3]])
        kernel = get_initial(shape = kernel_shape, name = name, load_option = load_option)
        bias = get_initial_bias(shape = out_shape[3], name = name + "_bias", load_option = load_option)
        result = tf.nn.conv2d_transpose(x, kernel, output_shape = deconv_shape,
                                        strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def depthwise_conv2d(x, kernel_shape, stride_size, pad, name, need_bn, in_training, load_option):
        kernel = get_initial(shape = kernel_shape, name = name, load_option = load_option)
        bias = get_initial_bias(shape = kernel_shape[3], name = name + "_bias", load_option = load_option)
        result = tf.nn.depthwise_conv2d(x, kernel, strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def separable_conv2d(x, kernel_shape, output_depth, stride_size, pad, name, need_bn, in_training, load_option):
        # f_h, f_w, input_depth, depth_multiplier
        depthwise_kernel = get_initial(shape = kernel_shape, name = name + "_depth", load_option = load_option)
        pointwise_kernel = get_initial(shape = [1, 1, kernel_shape[2] * kernel_shape[3], output_depth],
                                       name = name + "_point",
                                       load_option = load_option)
        bias = get_initial_bias(shape = output_depth, name = name + "_bias", load_option = load_option)
        result = tf.nn.separable_conv2d(x, depthwise_kernel, pointwise_kernel,
                                        strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def pool2d(x, k_size, stride, keep_prob, pad = "SAME"):
        return tf.nn.dropout(tf.nn.max_pool(x, ksize = [1, k_size, k_size, 1], strides = [1, stride, stride, 1],
                                            padding = pad), keep_prob)

    depth_1 = 64
    depth_2 = 64
    depth_3 = 64
    depth_4 = 64
    depth_5 = 64

    conv_1_1 = conv2d(x, [3, 3, x.shape[3], depth_1], 1, "SAME", "conv_1_1", False, training, load)
    conv_1_1 = tf.nn.relu(conv_1_1)
    conv_1_2 = conv2d(conv_1_1, [3, 3, conv_1_1.shape[3], depth_1], 1, "SAME", "conv_1_2", bn, training, load)
    conv_1_2 = tf.nn.relu(conv_1_2)
    conv_1_3 = conv2d(conv_1_2, [3, 3, conv_1_2.shape[3], depth_1], 1, "SAME", "conv_1_3", bn, training, load)
    conv_1_3 = tf.nn.relu(conv_1_3)
    pool_1 = pool2d(conv_1_3, 3, 2, keep_prob)

    conv_2_1 = conv2d(pool_1, [3, 3, pool_1.shape[3], depth_2], 1, "SAME", "conv_2_1", bn, training, load)
    conv_2_1 = tf.nn.relu(conv_2_1)
    conv_2_2 = conv2d(conv_2_1, [3, 3, conv_2_1.shape[3], depth_2], 1, "SAME", "conv_2_2", bn, training, load)
    conv_2_2 = tf.nn.relu(conv_2_2)
    conv_2_3 = conv2d(conv_2_2, [3, 3, conv_2_2.shape[3], depth_2], 1, "SAME", "conv_2_3", bn, training, load)
    conv_2_3 = tf.nn.relu(conv_2_3)
    pool_2 = pool2d(conv_2_3, 3, 2, keep_prob)

    conv_3_1 = conv2d(pool_2, [3, 3, pool_2.shape[3], depth_3], 1, "SAME", "conv_3_1", bn, training, load)
    conv_3_1 = tf.nn.relu(conv_3_1)
    conv_3_2 = conv2d(conv_3_1, [3, 3, conv_3_1.shape[3], depth_3], 1, "SAME", "conv_3_2", bn, training, load)
    conv_3_2 = tf.nn.relu(conv_3_2)
    conv_3_3 = conv2d(conv_3_2, [3, 3, conv_3_2.shape[3], depth_3], 1, "SAME", "conv_3_3", bn, training, load)
    conv_3_3 = tf.nn.relu(conv_3_3)
    pool_3 = pool2d(conv_3_3, 3, 2, keep_prob)

    conv_4_1 = conv2d(pool_3, [3, 3, pool_3.shape[3], depth_4], 1, "SAME", "conv_4_1", bn, training, load)
    conv_4_1 = tf.nn.relu(conv_4_1)
    conv_4_2 = conv2d(conv_4_1, [3, 3, conv_4_1.shape[3], depth_4], 1, "SAME", "conv_4_2", bn, training, load)
    conv_4_2 = tf.nn.relu(conv_4_2)
    conv_4_3 = conv2d(conv_4_2, [3, 3, conv_4_2.shape[3], depth_4], 1, "SAME", "conv_4_3", bn, training, load)
    conv_4_3 = tf.nn.relu(conv_4_3)
    pool_4 = pool2d(conv_4_3, 3, 2, keep_prob)

    conv_5_1 = conv2d(pool_4, [3, 3, pool_4.shape[3], depth_5], 1, "SAME", "conv_5_1", bn, training, load)
    conv_5_1 = tf.nn.relu(conv_5_1)
    conv_5_2 = conv2d(conv_5_1, [3, 3, conv_5_1.shape[3], depth_5], 1, "SAME", "conv_5_2", bn, training, load)
    conv_5_2 = tf.nn.relu(conv_5_2)
    conv_5_3 = separable_conv2d(conv_5_2, [3, 3, conv_5_2.shape[3], 1], depth_5, 1, "SAME", "conv_5_3", bn, training, load)
    conv_5_3 = tf.nn.relu(conv_5_3)
    transpose_1 = transpose_conv(conv_5_3, [3, 3, conv_4_2.shape[3], conv_5_3.shape[3]], 2, conv_4_2, "SAME", "transpose_1", bn, training, load)
    concat1 = tf.concat([transpose_1, conv_4_2], 3)
    concat1 = tf.nn.dropout(concat1, keep_prob)

    up_conv_1_1 = conv2d(concat1, [3, 3, concat1.shape[3], depth_4], 1, "SAME", "up_conv_1_1", bn, training, load)
    up_conv_1_1 = tf.nn.relu(up_conv_1_1)
    up_conv_1_2 = conv2d(up_conv_1_1, [3, 3, up_conv_1_1.shape[3], depth_4], 1, "SAME", "up_conv_1_2", bn, training, load)
    up_conv_1_2 = tf.nn.relu(up_conv_1_2)
    transpose_2 = transpose_conv(up_conv_1_2, [3, 3, conv_3_2.shape[3], up_conv_1_2.shape[3]], 2, conv_3_2, "SAME", "transpose_2", bn, training, load)
    concat2 = tf.concat([transpose_2, conv_3_2], 3)
    concat2 = tf.nn.dropout(concat2, keep_prob)

    up_conv_2_1 = conv2d(concat2, [3, 3, concat2.shape[3], depth_3], 1, "SAME", "up_conv_2_1", bn, training, load)
    up_conv_2_1 = tf.nn.relu(up_conv_2_1)
    up_conv_2_2 = conv2d(up_conv_2_1, [3, 3, up_conv_2_1.shape[3], depth_3], 1, "SAME", "up_conv_2_2", bn, training, load)
    up_conv_2_2 = tf.nn.relu(up_conv_2_2)
    transpose_3 = transpose_conv(up_conv_2_2, [3, 3, conv_2_2.shape[3], up_conv_2_2.shape[3]], 2, conv_2_2, "SAME", "transpose_3", bn, training, load)
    concat3 = tf.concat([transpose_3, conv_2_2], 3)
    concat3 = tf.nn.dropout(concat3, keep_prob)

    up_conv_3_1 = conv2d(concat3, [3, 3, concat3.shape[3], depth_2], 1, "SAME", "up_conv_3_1", bn, training, load)
    up_conv_3_1 = tf.nn.relu(up_conv_3_1)
    up_conv_3_2 = conv2d(up_conv_3_1, [3, 3, up_conv_3_1.shape[3], depth_2], 1, "SAME", "up_conv_3_2", bn, training, load)
    up_conv_3_2 = tf.nn.relu(up_conv_3_2)
    transpose_4 = transpose_conv(up_conv_3_2, [3, 3, conv_1_2.shape[3], up_conv_3_2.shape[3]], 2, conv_1_2, "SAME", "transpose_4", bn, training, load)
    concat4 = tf.concat([transpose_4, conv_1_2], 3)
    concat4 = tf.nn.dropout(concat4, keep_prob)

    up_conv_4_1 = conv2d(concat4, [3, 3, concat4.shape[3], depth_1], 1, "SAME", "up_conv_4_1", bn, training, load)
    up_conv_4_1 = tf.nn.relu(up_conv_4_1)
    up_conv_4_2 = conv2d(up_conv_4_1, [3, 3, up_conv_4_1.shape[3], depth_1], 1, "SAME", "up_conv_4_2", bn, training, load)
    up_conv_4_2 = tf.nn.relu(up_conv_4_2)
    final_conv = conv2d(up_conv_4_2, [3, 3, up_conv_4_2.shape[3], 1], 1, "SAME", "fianl_conv", bn, training, load)
    sigmoid = tf.nn.sigmoid(final_conv)
    return sigmoid


def get_IOU(prediction, ground_truth):
    H, W, _ = prediction.get_shape().as_list()[1:]
    prediction = tf.cast(tf.greater(prediction, 0.5), tf.float32)
    prediction_flatten = tf.reshape(prediction, shape = [-1, H * W])
    ground_truth_flatten = tf.reshape(ground_truth, shape = [-1, H * W])
    intersection = tf.reduce_sum(tf.cast(tf.multiply(prediction_flatten, ground_truth_flatten), tf.float32), axis = 1)
    union = tf.reduce_sum(ground_truth_flatten, axis = 1) + tf.reduce_sum(prediction_flatten, axis = 1) - intersection
    iou = tf.divide(tf.cast(intersection, tf.float32) + 1e-7, tf.cast(union, tf.float32) + 1e-7)
    return tf.reduce_mean(iou)


def get_optimizer(result, y, learning_rate):
    # result = tf.sigmoid(result)
    loss = tf.reduce_mean(tf.keras.backend.binary_crossentropy(y, result, from_logits = False))
    opt = tf.train.AdamOptimizer(learning_rate = learning_rate)
    # opt = tf.train.RMSPropOptimizer(learning_rate=learning_rate)
    train = opt.minimize(loss)
    return loss, train


def training_process(epoch_num, k_p, bn, output_path, load, model_path, x_path, y_path, learning_rate = 0.001):
    # training_data_x = read_image("D:\\kaggle\\salt\\train\\images", 3)
    # training_data_y = read_image("D:\\kaggle\\salt\\train\\masks", 1)
    training_data_x = np.load(x_path)
    training_data_y = np.load(y_path)
    training_data_x = np.reshape(training_data_x[:, :, :, 0], (training_data_x.shape[0], IMAGE_SIZE, IMAGE_SIZE, 1))
    training_data_y = np.reshape(training_data_y, (training_data_y.shape[0], IMAGE_SIZE, IMAGE_SIZE, 1))
    # training_data_x = training_data_x[0:8000,:,:,:]
    # training_data_y = training_data_y[0:8000,:,:,:]
    [x_train, x_val, y_train, y_val] = model_selection.train_test_split(training_data_x, training_data_y,
                                                                        test_size = VALIDATION_RATIO, random_state = 42)

    x = tf.placeholder(dtype = tf.float32, shape = [None, IMAGE_SIZE, IMAGE_SIZE, 1])
    y = tf.placeholder(dtype = tf.float32, shape = [None, IMAGE_SIZE, IMAGE_SIZE, 1])

    x_norm = (x / 127.5) - 1
    y_norm = (y / 255)
    training = tf.placeholder(dtype = tf.bool)
    keep_prob = tf.placeholder(dtype = tf.float32)

    sigmoid_result = create_model(x_norm, keep_prob, bn = bn, training = training, load = load)
    IOU = get_IOU(sigmoid_result, y_norm)
    cross_entropy, trainer = get_optimizer(sigmoid_result, y_norm, learning_rate = learning_rate)

    training_number = x_train.shape[0]
    val_number = x_val.shape[0]
    batch_num = math.ceil(training_number / BATCH_SIZE)
    batch_size_val = math.ceil(val_number / batch_num)
    loop_num = batch_num * epoch_num
    epoch_index = 0
    current_train_error = 0
    current_val_error = 0
    current_train_IOU = 0
    current_val_IOU = 0
    current_idx = 0
    val_idx = 0
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        if load:
            print("Load model from %s" %(model_path))
            saver = tf.train.Saver()
            saver.restore(sess, model_path)
        tStart = time.time()
        print_cur = 0
        for i in range(loop_num):
            current_batch = int(i % (batch_num) * BATCH_SIZE)
            next_batch = int((i % (batch_num) + 1) * BATCH_SIZE)

            current_batch_val = int(i % (batch_num) * batch_size_val)
            next_batch_val = int((i % (batch_num) + 1) * batch_size_val)

            if next_batch >= (training_number - 1):
                next_batch = training_number - 1
            if next_batch_val >= (val_number - 1):
                next_batch_val = val_number - 1

            batch_x_train = x_train[current_batch : next_batch, :, :, :]
            batch_y_train = y_train[current_batch: next_batch, :, :, :]
            [train_error, feed, IOU_train] = sess.run([cross_entropy, trainer, IOU],
                                           {x: batch_x_train, y: batch_y_train, training: True, keep_prob: k_p})
            current_train_error += train_error
            current_train_IOU += IOU_train
            current_idx += 1

            if current_batch_val < (val_number - 1):
                batch_x_val = x_val[current_batch_val : next_batch_val, :, :, :]
                batch_y_val = y_val[current_batch_val: next_batch_val, :, :, :]
                [val_error, IOU_val] = sess.run([cross_entropy, IOU], {x: batch_x_val, y: batch_y_val, training: False, keep_prob : 1})
                current_val_error += val_error
                current_val_IOU += IOU_val
                val_idx += 1

            if print_cur:
                print("Current train_step = %s, epoch = %s " % (current_idx, epoch_index))
                print("Current train_error = %s" % (train_error))
                print("Current val_error = %s\n" % (val_error))
                print("Current train_IOU = %s" % (IOU_train))
                print("Current val_IOU = %s\n" % (IOU_val))

            if next_batch == (training_number - 1):
                epoch_index += 1
                train_error_mean = current_train_error / current_idx
                val_error_mean = current_val_error / val_idx
                train_IOU_mean = current_train_IOU / current_idx
                val_IOU_mean = current_val_IOU / val_idx
                print("*-----------------------------------This is epoch %s----------------------------------*" % (epoch_index))
                print("Mean train_error = %s" % (train_error_mean))
                print("Mean val_error = %s" % (val_error_mean))
                print("Mean train_IOU = %s" % (train_IOU_mean))
                print("Mean val_IOU = %s" % (val_IOU_mean))
                print("*------------------------------------------------------------------------------------*")
                current_train_error = 0
                current_val_error = 0
                current_train_IOU = 0
                current_val_IOU = 0
                current_idx = 0
                val_idx = 0

        tEnd = time.time()
        print("Training time = %s sec" % (tEnd - tStart))

        saver = tf.train.Saver()
        ckpt_path = output_path + "\\ckpt"
        if not os.path.exists(ckpt_path):
            os.makedirs(ckpt_path)
        graph_path = output_path + "\\model"
        if not os.path.exists(graph_path):
            os.makedirs(graph_path)
        save_path = saver.save(sess, ckpt_path + "\\salt.ckpt")
        tf.train.write_graph(sess.graph_def, graph_path, "model.pbtxt", as_text = True)
        print("Model saved in file: %s" % save_path)


def testing(testing_path, model_path, output_path):
    x = tf.placeholder(dtype = tf.float32, shape = [None, IMAGE_SIZE, IMAGE_SIZE, 1])
    y = tf.placeholder(dtype=tf.float32, shape=[None, IMAGE_SIZE, IMAGE_SIZE, 1])
    x_norm = (x / 127.5) - 1
    y_norm = (y / 255)
    training = tf.placeholder(dtype = tf.bool)
    keep_prob = tf.placeholder(dtype = tf.float32)
    sigmoid_result = create_model(x_norm, keep_prob, bn = False, training = training, load = True)
    result_resize = tf.image.resize_bilinear(sigmoid_result, size = [101, 101])
    prediction = tf.cast(tf.greater(result_resize, 0.5), tf.float32)
    list = os.listdir(testing_path)
    counter = 0

    a = np.int32(1)
    b = np.float32(1)
    test_dict = {"id":[a], "rle_mask":[b]}
    test = pd.DataFrame(test_dict)
    test = test.drop(0)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print("Load model from %s" %(model_path))
        saver = tf.train.Saver()
        saver.restore(sess, model_path)
        for filename in list:
            image_name = (filename[:-4])
            img = Image.open(testing_path + "\\" + filename)
            img_resize = img.resize((IMAGE_SIZE, IMAGE_SIZE), Image.BILINEAR)
            img_np = np.asarray(img_resize, dtype=np.uint8)
            img_np = np.reshape(img_np[:, :, 0], (1, IMAGE_SIZE, IMAGE_SIZE, 1))
            cur_prediction = sess.run([prediction], {x: img_np, training: False, keep_prob: 1})
            predict_np = np.asarray(cur_prediction)
            code = rle_encoding(predict_np)
            code_string = ''
            for j in code:
                code_string += '{} '.format(j)
            code_string = code_string[:-1]
            test_dict_ = {"id": [image_name], "rle_mask": [code_string]}
            test_ = pd.DataFrame(test_dict_)
            combine = [test, test_]
            test = pd.concat(combine)
            print("This is no.%s image, image name = %s" % (counter, image_name))
            counter += 1
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        test.to_csv(output_path + "\\predict.csv", index=False)


def read_image2(img_path):
    i = 0
    list = os.listdir(img_path)
    number_files = len(list)
    train_x = [None] * number_files
    for filename in (list):
        image_name = (filename[:-4])
        img = Image.open(img_path + "\\" + filename)
        img_resize = img.resize((IMAGE_SIZE, IMAGE_SIZE), Image.BILINEAR)
        img_np = np.asarray(img_resize, dtype=np.uint8)
        if (img_np.ndim == 2):
            img_np = np.expand_dims(img_np, axis = 2)
        img_reshape = np.reshape(img_np[:, :, 0], newshape = (IMAGE_SIZE, IMAGE_SIZE, 1))
        train_x[i] = img_reshape
        print("This is no.%s image, image name = %s" % (i, image_name))
        i += 1
    data_set = np.asarray(train_x, dtype=np.uint8)
    return data_set


def data_augmentation(ori_path, aug_path, output_path):
    ori_data = read_image2(ori_path)
    aug_data = read_image2(aug_path)
    total = np.vstack((ori_data, aug_data))
    np.save(output_path, total)


def create_np(ori_path, output_path):
    ori_data = read_image2(ori_path)
    np.save(output_path, ori_data)

def create_aug(image_path, mask_path, output_path, aug_num):
    if not os.path.exists(output_path + "\\images"):
        os.makedirs(output_path + "\\images")
    else:
        shutil.rmtree(output_path + "\\images")
        os.makedirs(output_path + "\\images")

    if not os.path.exists(output_path + "\\masks"):
        os.makedirs(output_path + "\\masks")
    else:
        shutil.rmtree(output_path + "\\masks")
        os.makedirs(output_path + "\\masks")

    p = Augmentor.Pipeline(image_path, output_directory=output_path + "\\images")
    p.ground_truth(mask_path)
    p.random_distortion(probability=0.75, grid_width=5, grid_height=5, magnitude=10)
    p.flip_left_right(probability=0.75)
    #p.flip_top_bottom(probability=0.5)
    p.sample(aug_num)
    list = os.listdir(output_path + "\\images")
    for filename in (list):
        if (filename[0:7] == '_ground'):
            shutil.move(output_path + "\\images\\" + filename, output_path + "\\masks\\" + filename)
    # p.process()


def main(gen_data, training, predict):
    x_path = "D:\\kaggle\\salt\\train\\augmented_x.npy"
    y_path = "D:\\kaggle\\salt\\train\\augmented_y.npy"
    ori_image_path = "D:\\kaggle\\salt\\train\\separate\\train"
    ori_mask_path = "D:\\kaggle\\salt\\train\\separate\\train_mask"
    aug_path = "D:\\kaggle\\salt\\train\\augmentation"
    output_model_path = "D:\\kaggle\\salt"
    # create_np("D:\\kaggle\\salt\\train\\separate\\validation", "D:\\kaggle\\salt\\train\\separate\\validation_x.npy")
    # create_np("D:\\kaggle\\salt\\train\\separate\\validation_mask", "D:\\kaggle\\salt\\train\\separate\\validation_y.npy")
    if gen_data:
        create_aug(ori_image_path, ori_mask_path, aug_path, 3250 * 4)
        data_augmentation(ori_image_path, aug_path + "\\images",  x_path)
        data_augmentation(ori_mask_path, aug_path + "\\masks", y_path)

    if training:
        training_process(15, 0.5, bn = False, output_path = output_model_path, load = False,
                         model_path = output_model_path + "\\ckpt\\salt.ckpt",
                         x_path = x_path, y_path = y_path, learning_rate = 0.0005)
    if predict:
        testing(testing_path = "D:\\kaggle\\salt\\test", model_path = "D:\\kaggle\\salt\\testing_model\\ckpt\\salt.ckpt",
                output_path = "D:\\kaggle\\salt\\predict")
if __name__ == '__main__':
    main(gen_data = False, training = True, predict = False)