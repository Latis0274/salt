# from scipy import misc
import matplotlib.pyplot as plt
#########################################
from PIL import Image
import matplotlib.image as mpimg
import Augmentor
import pandas as pd
########################################
import os
from sklearn import model_selection
import shutil
import numpy as np
import tensorflow as tf
from tensorflow.contrib.slim.nets import resnet_v1
import tensorflow.contrib.slim as slim
import time
import math


BATCH_SIZE = 32
IMAGE_SIZE = 101
NEW_IMAGE_SIZE = 128
VALIDATION_RATIO = 0.2


# date layout is [index, how many ones, index, how many ones......], even index: start position of one, odd index: how many consecutive ones
# note that the starting index is 1, not 0
# example: 10010000111, rle encoding = 1 1, 4 1,  9 3
def rle_encoding(img):
    # along column
    pixels = img.flatten('F')
    # pad zero because the we want our first finding index is where 0->1, and last index is where 1->0(see below)
    pixels = np.concatenate([[0], pixels, [0]])
    # find index where current number is not equaling to previous number
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    # runs[1] = runs[1] - runs[0], runs[3] = runs[3] - runs[2]....... this procedure is calculating the number of consecutive ones
    runs[1::2] -= runs[::2]
    return runs


def read_image(image_path, input_channel):
    list = os.listdir(image_path)
    number_files = len(list)
    # number_files = 1
    counter = 0
    dataset = np.zeros(shape=[number_files, IMAGE_SIZE, IMAGE_SIZE, input_channel])
    for filename in list:
        image_name = (filename[:-4])
        img = mpimg.imread(image_path + "/" + filename)
        if (img.ndim == 2):
            img = np.expand_dims(img, axis = 2)
        dataset[counter] = img
        counter += 1
        # print("This is no.%s image, image_name is %s" %(counter, image_name))
        # imgplot = plt.imshow(img)
        # plt.show()
        # if (counter == number_files):
    return np.reshape(dataset[:, :, :, 0], (number_files, IMAGE_SIZE, IMAGE_SIZE, 1))


def read_image2(img_path):
    i = 0
    list = os.listdir(img_path)
    number_files = len(list)
    train_x = [None] * number_files
    for filename in (list):
        image_name = (filename[:-4])
        img = Image.open(img_path + "/" + filename)
        img_np = np.asarray(img, dtype=np.uint8)
        if (img_np.ndim == 2):
            img_np = np.expand_dims(img_np, axis = 2)
        img_reshape = np.reshape(img_np[:, :, 0], newshape = (IMAGE_SIZE, IMAGE_SIZE, 1))
        train_x[i] = img_reshape
        # print("This is no.%s image, image name = %s" % (i, image_name))
        i += 1
    data_set = np.asarray(train_x, dtype=np.uint8)
    return data_set

def create_encoder(sess, x, training):
    with slim.arg_scope(resnet_v1.resnet_arg_scope()):
        net, end_points = resnet_v1.resnet_v1_50(x, is_training=training)
    x_input = sess.graph.get_tensor_by_name("Placeholder:0")
    pre_op1 = sess.graph.get_operation_by_name("resnet_v1_50/conv1/Relu").outputs[0]
    pre_op2 = sess.graph.get_operation_by_name("resnet_v1_50/block1/unit_2/bottleneck_v1/Relu").outputs[0]
    pre_op3 = sess.graph.get_operation_by_name("resnet_v1_50/block2/unit_3/bottleneck_v1/Relu").outputs[0]
    pre_op4 = sess.graph.get_operation_by_name("resnet_v1_50/block3/unit_5/bottleneck_v1/Relu").outputs[0]
    final = sess.graph.get_operation_by_name("resnet_v1_50/block4/unit_3/bottleneck_v1/Relu").outputs[0]
    return x_input, pre_op1, pre_op2, pre_op3, pre_op4, final


def create_decoder(pre_op1, pre_op2, pre_op3, pre_op4, final, name, keep_prob, bn, training, load):
    def get_initial(shape, name, load_option):
        if load_option:
            return tf.get_variable(name = name, shape = shape)
        else:
            return tf.get_variable(name = name, shape = shape, initializer = tf.keras.initializers.glorot_uniform())

    def get_initial_bias(shape, name, load_option):
        if load_option:
            return tf.get_variable(name = name, shape = shape)
        else:
            return tf.get_variable(name = name, shape = shape, initializer=tf.zeros_initializer())

    def batch_norm(x, training):
        return tf.layers.batch_normalization(x, center = True, scale = True, training = training, momentum = 0.90)

    def conv2d(x, kernel_shape, stride_size, pad, name, need_bn, in_training, load_option):
        kernel = get_initial(shape = kernel_shape, name = name, load_option = load_option)
        bias = get_initial_bias(shape = kernel_shape[3], name = name + "_bias", load_option = load_option)
        result = tf.nn.conv2d(x, kernel, strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def transpose_conv(x, kernel_shape, stride_size, out_tensor, pad, name, need_bn, in_training, load_option):
        out_shape = out_tensor.get_shape().as_list()
        batch_size = tf.shape(x)[0]
        deconv_shape = tf.stack([batch_size, out_shape[1], out_shape[2], out_shape[3]])
        kernel = get_initial(shape = kernel_shape, name = name, load_option = load_option)
        bias = get_initial_bias(shape = out_shape[3], name = name + "_bias", load_option = load_option)
        result = tf.nn.conv2d_transpose(x, kernel, output_shape = deconv_shape,
                                        strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def transpose_conv2(x, kernel_shape, stride_size, out_shape, pad, name, need_bn, in_training, load_option):
        batch_size = tf.shape(x)[0]
        deconv_shape = tf.stack([batch_size, out_shape[1], out_shape[2], out_shape[3]])
        kernel = get_initial(shape = kernel_shape, name = name, load_option = load_option)
        bias = get_initial_bias(shape = out_shape[3], name = name + "_bias", load_option = load_option)
        result = tf.nn.conv2d_transpose(x, kernel, output_shape = deconv_shape,
                                        strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def depthwise_conv2d(x, kernel_shape, stride_size, pad, name, need_bn, in_training, load_option):
        kernel = get_initial(shape = kernel_shape, name = name, load_option = load_option)
        bias = get_initial_bias(shape = kernel_shape[3], name = name + "_bias", load_option = load_option)
        result = tf.nn.depthwise_conv2d(x, kernel, strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def separable_conv2d(x, kernel_shape, output_depth, stride_size, pad, name, need_bn, in_training, load_option):
        # f_h, f_w, input_depth, depth_multiplier
        depthwise_kernel = get_initial(shape = kernel_shape, name = name + "_depth", load_option = load_option)
        pointwise_kernel = get_initial(shape = [1, 1, kernel_shape[2] * kernel_shape[3], output_depth],
                                       name = name + "_point",
                                       load_option = load_option)
        bias = get_initial_bias(shape = output_depth, name = name + "_bias", load_option = load_option)
        result = tf.nn.separable_conv2d(x, depthwise_kernel, pointwise_kernel,
                                        strides = [1, stride_size, stride_size, 1], padding = pad) + bias
        if need_bn:
            return batch_norm(result, in_training)
        else:
            return result

    def pool2d(x, k_size, stride, keep_prob, pad = "SAME"):
        return tf.nn.dropout(tf.nn.max_pool(x, ksize = [1, k_size, k_size, 1], strides = [1, stride, stride, 1],
                                            padding = pad), keep_prob)

    depth_0 = 64
    depth_1 = 64
    depth_2 = 128
    depth_3 = 256
    depth_4 = 512
    depth_5 = 1024

    with tf.variable_scope(name) as scope:
        conv_5_1 = conv2d(final, [3, 3, final.shape[3], depth_5], 1, "SAME", "conv_5_1", bn, training, load)
        conv_5_1 = tf.nn.relu(conv_5_1)
        conv_5_2 = conv2d(conv_5_1, [3, 3, conv_5_1.shape[3], depth_5], 1, "SAME", "conv_5_2", bn, training, load)
        conv_5_2 = tf.nn.relu(conv_5_2)
        conv_5_3 = conv2d(conv_5_2, [3, 3, conv_5_2.shape[3], depth_5], 1, "SAME", "conv_5_3", bn, training, load)
        conv_5_3 = tf.nn.relu(conv_5_3)
        transpose_1 = transpose_conv(conv_5_3, [3, 3, pre_op4.shape[3], conv_5_3.shape[3]], 2, pre_op4, "SAME",
                                     "transpose_1", bn, training, load)
        concat1 = tf.concat([transpose_1, pre_op4], 3)
        concat1 = tf.nn.dropout(concat1, keep_prob)

        up_conv_1_1 = conv2d(concat1, [3, 3, concat1.shape[3], depth_4], 1, "SAME", "up_conv_1_1", bn, training, load)
        up_conv_1_1 = tf.nn.relu(up_conv_1_1)
        up_conv_1_2 = conv2d(up_conv_1_1, [3, 3, up_conv_1_1.shape[3], depth_4], 1, "SAME", "up_conv_1_2", bn, training,
                             load)
        up_conv_1_2 = tf.nn.relu(up_conv_1_2)
        transpose_2 = transpose_conv(up_conv_1_2, [3, 3, pre_op3.shape[3], up_conv_1_2.shape[3]], 2, pre_op3, "SAME",
                                     "transpose_2", bn, training, load)
        concat2 = tf.concat([transpose_2, pre_op3], 3)
        concat2 = tf.nn.dropout(concat2, keep_prob)

        up_conv_2_1 = conv2d(concat2, [3, 3, concat2.shape[3], depth_3], 1, "SAME", "up_conv_2_1", bn, training, load)
        up_conv_2_1 = tf.nn.relu(up_conv_2_1)
        up_conv_2_2 = conv2d(up_conv_2_1, [3, 3, up_conv_2_1.shape[3], depth_3], 1, "SAME", "up_conv_2_2", bn, training,
                             load)
        up_conv_2_2 = tf.nn.relu(up_conv_2_2)
        transpose_3 = transpose_conv(up_conv_2_2, [3, 3, pre_op2.shape[3], up_conv_2_2.shape[3]], 2, pre_op2, "SAME",
                                     "transpose_3", bn, training, load)
        concat3 = tf.concat([transpose_3, pre_op2], 3)
        concat3 = tf.nn.dropout(concat3, keep_prob)

        up_conv_3_1 = conv2d(concat3, [3, 3, concat3.shape[3], depth_2], 1, "SAME", "up_conv_3_1", bn, training, load)
        up_conv_3_1 = tf.nn.relu(up_conv_3_1)
        up_conv_3_2 = conv2d(up_conv_3_1, [3, 3, up_conv_3_1.shape[3], depth_2], 1, "SAME", "up_conv_3_2", bn, training,
                             load)
        up_conv_3_2 = tf.nn.relu(up_conv_3_2)
        transpose_4 = transpose_conv(up_conv_3_2, [3, 3, pre_op1.shape[3], up_conv_3_2.shape[3]], 2, pre_op1, "SAME",
                                     "transpose_4", bn, training, load)
        concat4 = tf.concat([transpose_4, pre_op1], 3)
        concat4 = tf.nn.dropout(concat4, keep_prob)

        up_conv_4_1 = conv2d(concat4, [3, 3, concat4.shape[3], depth_1], 1, "SAME", "up_conv_4_1", bn, training, load)
        up_conv_4_1 = tf.nn.relu(up_conv_4_1)
        up_conv_4_2 = conv2d(up_conv_4_1, [3, 3, up_conv_4_1.shape[3], depth_1], 1, "SAME", "up_conv_4_2", bn, training,
                             load)
        up_conv_4_2 = tf.nn.relu(up_conv_4_2)
        transpose_5 = transpose_conv2(up_conv_4_2, [3, 3, depth_1, up_conv_4_2.shape[3]], 2,
                                      [-1, NEW_IMAGE_SIZE, NEW_IMAGE_SIZE, depth_1], "SAME",
                                      "transpose_5", bn, training, load)

        up_conv_5_1 = conv2d(transpose_5, [3, 3, transpose_5.shape[3], depth_0], 1, "SAME", "up_conv_5_1", bn, training,
                             load)
        up_conv_5_1 = tf.nn.relu(up_conv_5_1)
        up_conv_5_2 = conv2d(up_conv_5_1, [3, 3, up_conv_5_1.shape[3], depth_0], 1, "SAME", "up_conv_5_2", bn, training,
                             load)
        up_conv_5_2 = tf.nn.relu(up_conv_5_2)
        final_conv = conv2d(up_conv_5_2, [1, 1, up_conv_5_2.shape[3], 1], 1, "SAME", "fianl_conv", False, training,
                            load)
        return final_conv


def get_IOU(prediction, ground_truth):
    H, W, _ = prediction.get_shape().as_list()[1:]
    prediction_flatten = tf.reshape(prediction, shape = [-1, H * W])
    ground_truth_flatten = tf.reshape(ground_truth, shape = [-1, H * W])
    intersection = tf.reduce_sum(tf.cast(tf.multiply(prediction_flatten, ground_truth_flatten), tf.float32), axis = 1)
    union = tf.reduce_sum(ground_truth_flatten, axis = 1) + tf.reduce_sum(prediction_flatten, axis = 1) - intersection
    iou = tf.divide(tf.cast(intersection, tf.float32) + 1e-7, tf.cast(union, tf.float32) + 1e-7)
    return tf.reduce_mean(iou)


def dice_coef(prediction, ground_truth):
    smooth = 1e-5
    prediction = tf.nn.sigmoid(prediction)
    intersection = tf.reduce_sum(prediction * ground_truth)
    union = tf.reduce_sum(prediction) + tf.reduce_sum(ground_truth)
    loss = (2 * intersection + smooth)/ (union + smooth)
    return loss


def get_optimizer(result, y, name, learning_rate):
    bce = tf.reduce_mean(tf.keras.backend.binary_crossentropy(y, result, from_logits = True))
    dice_score = dice_coef(result, y)
    loss = bce - tf.log(dice_score)
    opt = tf.train.AdamOptimizer(learning_rate = learning_rate)
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, name)
    # update_ops += tf.get_collection(tf.GraphKeys.UPDATE_OPS,
    #                                'resnet_v1_50')
    train_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
                                   name)
    with tf.control_dependencies(update_ops):
        # train = opt.minimize(loss, var_list = train_vars)
        train = opt.minimize(loss)
        return loss, train


def flip_and_pad(images, need_flip):
    value1 = math.ceil((NEW_IMAGE_SIZE - IMAGE_SIZE) / 2)
    value2 = math.floor((NEW_IMAGE_SIZE - IMAGE_SIZE) / 2)
    if need_flip:
        flip = images[:, :, ::-1, :]
        concat = np.concatenate((images, flip), 0)
        result = np.pad(concat, ((0, 0), (value1, value2), (value1, value2), (0, 0)), 'symmetric')
        return result
    else:
        result = np.pad(images, ((0, 0), (value1, value2), (value1, value2), (0, 0)), 'symmetric')
        return result


def concat3(images):
    return np.concatenate((images, images, images), axis = 3)


def preprocess(images, is_x, is_train):
    if is_x:
        result = concat3(flip_and_pad(images, is_train))
        return result
    else:
        result = flip_and_pad(images, is_train)
        return result


def training_process(epoch_num, k_p, bn, output_path, load, model_path, x_path, y_path, resnet_path, learning_rate = 0.001):

    training_data_x = read_image2(x_path)
    training_data_y = read_image2(y_path)

    [x_remain, x_train1, y_remain, y_train1] = model_selection.train_test_split(training_data_x, training_data_y,
                                                                        test_size = 0.2, random_state = 42)
    [x_remain, x_train2, y_remain, y_train2] = model_selection.train_test_split(x_remain, y_remain,
                                                                        test_size = 0.25, random_state = 9527)
    [x_remain, x_train3, y_remain, y_train3] = model_selection.train_test_split(x_remain, y_remain,
                                                                        test_size = 0.33, random_state = 1990)
    [x_train5, x_train4, y_train5, y_train4] = model_selection.train_test_split(x_remain, y_remain,
                                                                        test_size = 0.5, random_state = 2018)
    x_concat = np.concatenate((x_train4, x_train2, x_train3, x_train5), 0)
    y_concat = np.concatenate((y_train4, y_train2, y_train3, y_train5), 0)
    x_train = preprocess(x_concat, True, True)
    y_train = preprocess(y_concat, False, True)
    x_val = preprocess(x_train1, True, False)
    y_val = preprocess(y_train1, False, False)

    x = tf.placeholder(dtype = tf.float32, shape = [None, NEW_IMAGE_SIZE, NEW_IMAGE_SIZE, 3])
    y = tf.placeholder(dtype = tf.float32, shape = [None, NEW_IMAGE_SIZE, NEW_IMAGE_SIZE, 1])

    x_norm = (x / 127.5) - 1
    y_norm = (y / 255)
    training = tf.placeholder(dtype = tf.bool)
    keep_prob = tf.placeholder(dtype = tf.float32)
    with tf.Session() as sess:
        [x_input, pre_op1, pre_op2, pre_op3, pre_op4, final] = create_encoder(sess, x_norm, training = training)

        final_conv = create_decoder(pre_op1, pre_op2, pre_op3, pre_op4, final, "decoder", keep_prob, bn = bn, training = training, load = load)
        cross_entropy, trainer = get_optimizer(final_conv, y_norm, "decoder", learning_rate=learning_rate)
        sigmoid_result = tf.nn.sigmoid(final_conv)
        prediction = tf.cast(tf.greater(sigmoid_result, 0.5), tf.float32)
        IOU = get_IOU(prediction, y_norm)

        training_number = x_train.shape[0]
        val_number = x_val.shape[0]
        batch_num = math.ceil(training_number / BATCH_SIZE)
        batch_size_val = math.ceil(val_number / batch_num)
        loop_num = batch_num * epoch_num
        epoch_index = 0

        current_train_error = 0
        current_val_error = 0
        current_train_IOU = 0
        current_val_IOU = 0

        current_idx = 0
        val_idx = 0

        sess.run(tf.global_variables_initializer())
        all_var = tf.global_variables()
        if load:
            restore_list = [var for var in all_var if (var.name.split('/')[0] == 'resnet_v1_50' or
                                                       # var.name.split('/')[0] == 'decoder1' or
                                                       # var.name.split('/')[0] == 'decoder2' or
                                                       var.name.split('/')[0] == 'decoder')
                            and var.name.split('/')[-1][:4] != 'Adam']
            saver = tf.train.Saver(restore_list)
            saver.restore(sess, model_path)
            print("Load model from %s" % (model_path))
        else:
            restore_list = [var for var in all_var if (var.name.split('/')[0] == 'resnet_v1_50')
                            and var.name.split('/')[-1][:4] != 'Adam']
            saver = tf.train.Saver(restore_list)
            saver.restore(sess, resnet_path)
            print("Load model from %s" % (resnet_path))

        tStart = time.time()
        for i in range(loop_num):
            current_batch = int(i % (batch_num) * BATCH_SIZE)
            next_batch = int((i % (batch_num) + 1) * BATCH_SIZE)
            current_batch_val = int(i % (batch_num) * batch_size_val)
            next_batch_val = int((i % (batch_num) + 1) * batch_size_val)

            if next_batch >= (training_number - 1):
                next_batch = training_number - 1
            if next_batch_val >= (val_number - 1):
                next_batch_val = val_number - 1
            batch_x_train = x_train[current_batch : next_batch, :, :, :]
            batch_y_train = y_train[current_batch: next_batch, :, :, :]
            [train_error, feed, IOU_train] = sess.run([cross_entropy, trainer, IOU],
                                           {x_input: batch_x_train, y: batch_y_train, training: True, keep_prob: k_p})
            current_train_error += train_error
            current_train_IOU += IOU_train
            current_idx += 1
            if current_batch_val < (val_number - 1):
                batch_x_val = x_val[current_batch_val : next_batch_val, :, :, :]
                batch_y_val = y_val[current_batch_val: next_batch_val, :, :, :]
                [val_error, IOU_val] = sess.run([cross_entropy, IOU],
                                                {x_input: batch_x_val, y: batch_y_val, training: False, keep_prob: 1})
                current_val_error += val_error
                current_val_IOU += IOU_val
                val_idx += 1

            if next_batch == (training_number - 1):
                epoch_index += 1
                train_error_mean = current_train_error / current_idx
                val_error_mean = current_val_error / val_idx
                train_IOU_mean = current_train_IOU / current_idx
                val_IOU_mean = current_val_IOU / val_idx
                print("*-----------------------------------This is epoch %s----------------------------------*" % (epoch_index))
                print("Mean train_error = %s" % (train_error_mean))
                print("Mean val_error = %s" % (val_error_mean))
                print("Mean train_IOU = %s" % (train_IOU_mean))
                print("Mean val_IOU = %s" % (val_IOU_mean))
                print("*------------------------------------------------------------------------------------*")
                current_train_error = 0
                current_val_error = 0
                current_train_IOU = 0
                current_val_IOU = 0
                current_idx = 0
                val_idx = 0
        tEnd = time.time()
        print("Training time = %s sec" % (tEnd - tStart))
        all_var = tf.global_variables()
        save_list =  [var for var in all_var if (var.name.split('/')[0] == 'resnet_v1_50' or
                                                 # var.name.split('/')[0] == 'decoder1' or
                                                 # var.name.split('/')[0] == 'decoder2' or
                                                 var.name.split('/')[0] == 'decoder')
                      and var.name.split('/')[-1][:4] != 'Adam']
        saver = tf.train.Saver(save_list)
        ckpt_path = output_path + "/ckpt"
        if not os.path.exists(ckpt_path):
            os.makedirs(ckpt_path)
        graph_path = output_path + "/model"
        if not os.path.exists(graph_path):
            os.makedirs(graph_path)
        save_path = saver.save(sess, ckpt_path + "/salt.ckpt")
        tf.train.write_graph(sess.graph_def, graph_path, "model.pbtxt", as_text = True)
        print("Model saved in file: %s" % save_path)


def testing(testing_path, model_path, output_path):
    x = tf.placeholder(dtype = tf.float32, shape = [None, NEW_IMAGE_SIZE, NEW_IMAGE_SIZE, 3])
    x_norm = (x / 127.5) - 1
    training = tf.placeholder(dtype = tf.bool)
    keep_prob = tf.placeholder(dtype = tf.float32)

    with tf.Session() as sess:
        [x_input, pre_op1, pre_op2, pre_op3, pre_op4, final] = create_encoder(sess, x_norm, training=training)

        final_conv = create_decoder(pre_op1, pre_op2, pre_op3, pre_op4, final, "decoder", keep_prob, bn = True, training = training, load = True)
        sigmoid_result = tf.nn.sigmoid(final_conv)
        prediction = tf.cast(tf.greater(sigmoid_result, 0.5), tf.float32)

        list = os.listdir(testing_path)
        number_files = len(list)

        testing_x = read_image2(testing_path)
        testing_x = preprocess(testing_x, True, False)
        avg_result = np.zeros((number_files, IMAGE_SIZE, IMAGE_SIZE, 1), dtype=np.float32)

        a = np.int32(1)
        b = np.float32(1)
        test_dict = {"id":[a], "rle_mask":[b]}
        test = pd.DataFrame(test_dict)
        test = test.drop(0)
        for k in range(5):
            sess.run(tf.global_variables_initializer())
            all_var = tf.global_variables()
            restore_list = [var for var in all_var if (var.name.split('/')[0] == 'resnet_v1_50' or
                                                       var.name.split('/')[0] == 'decoder')
                            and var.name.split('/')[-1][:4] != 'Adam']
            saver = tf.train.Saver(restore_list)
            new_path = model_path + str(k + 1) + "/ckpt/salt.ckpt"
            saver.restore(sess, new_path)
            print("Load model from %s" % new_path)
            for i in range(number_files):
                input = np.reshape(testing_x[i, :, :, :], (1, NEW_IMAGE_SIZE, NEW_IMAGE_SIZE, 3))
                cur_prediction = sess.run([sigmoid_result], {x_input: input, training: False, keep_prob: 1})
                predict_np = np.squeeze(np.array(cur_prediction, dtype=np.float32), (0, 1))
                value1 = math.ceil((NEW_IMAGE_SIZE - IMAGE_SIZE) / 2)
                predict_np = predict_np[value1 : value1 + IMAGE_SIZE, value1 : value1 + IMAGE_SIZE, :]
                avg_result[i] += predict_np
        print("prediction done")
        avg_result /= 5
        avg_result = np.round(avg_result)
        print("start generating csv")
        for i in range(number_files):
            if i % 1000 == 0:
                print("%s/18000" %i)
            code = rle_encoding(avg_result[i])
            code_string = ''
            for j in code:
                code_string += '{} '.format(j)
            code_string = code_string[:-1]
            image_name = (list[i][:-4])
            test_dict_ = {"id": [image_name], "rle_mask": [code_string]}
            test_ = pd.DataFrame(test_dict_)
            combine = [test, test_]
            test = pd.concat(combine)

        if not os.path.exists(output_path):
            os.makedirs(output_path)
        test.to_csv(output_path + "/predict.csv", index=False)


def main(training, predict):
    x_path = "/proj/gpu_mtk15475/train/images"
    y_path = "/proj/gpu_mtk15475/train/masks"
    output_model_path = "/proj/gpu_mtk15475/train/output"
    resnet_path = "/proj/gpu_mtk15475/train/resnet_v1_50_2016_08_28/resnet_v1_50.ckpt"
    # resnet_path = "/proj/gpu_mtk15475/train/finetune/salt.ckpt"
    if training:
        training_process(10, 0.5, bn = True, output_path = output_model_path, load = True,
                         model_path = output_model_path + "/ckpt/salt.ckpt",
                         x_path = x_path, y_path = y_path, resnet_path = resnet_path, learning_rate = 0.0003)
    if predict:
        testing(testing_path = "/proj/gpu_mtk15475/train/test/images", model_path = "/proj/gpu_mtk15475/train/k-fold/",
                output_path = "/proj/gpu_mtk15475/train/test/predict")
if __name__ == '__main__':
    main(training = False, predict = True)
